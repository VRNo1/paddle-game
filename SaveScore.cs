using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class SaveScore : MonoBehaviour {
	
	// Save score variables
	private StreamWriter sw;
	private StreamReader sr;
	private List<string> currentScores;
	
	public int LeaderboardLength = 10; 

	// Use this for initialization
	void Start () {
	
	}
	
	void Awake() {
		currentScores = new List<string>();
		currentScores = GetHighScoreList();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public List<string> GetHighScoreList() {
		// Get scores from file.
		//string scores = ReadFromFile();
		// Split into arraylist.
		//string[] currentScores = scores.Split("\n"[0]);
		// Initiate a temporary list.
		List<string> tempList = ReadFromFile();//new List<string>();
		// Fill the temporary list.
		/*foreach(string score in currentScores) {
			tempList.Add(score);
		}*/
		// return values.
		return tempList;
	}
	
	public void SaveHighScore(int newScore) {
		// Variable to test if the highscore has changed, if not dont resave data.
		bool changed = false;
		// Iterate through highscore values and compare to new value.
		for (int i = 1; i <= currentScores.Count && i <= LeaderboardLength; i++) {
			if(newScore > int.Parse(currentScores[i - 1])) {
				Debug.Log("new score: " + newScore);
				changed = true;
				currentScores.Insert(i - 1, newScore.ToString());
				break;
			}
			if (i == currentScores.Count && i < LeaderboardLength) {
				changed = true;
				currentScores.Add(newScore.ToString());
				break;
			}
		}
		// If the highscore has changed save the new values.
		if(changed) {
			WriteToFile();
		}
	}
	
	/**
	 * Read data from file.
	 */
	private List<string> ReadFromFile() {
		List<string>HighScores = new List<string>();
		
		int i = 1;
		while (i <= LeaderboardLength) {
			string temp = "0";
			if(PlayerPrefs.HasKey("HighScore" + i + "name")) {
				temp = PlayerPrefs.GetString("HighScore" + i + "name");
			}
			HighScores.Add(temp);
			i++;
		}
		
		return HighScores;
		/*
		sr = new StreamReader(Application.dataPath + "/Saved_High_Score.txt");
		string fileContents = sr.ReadToEnd();
		sr.Close();
		return fileContents;
		*/
	}
	
	/**
	 * Save data to file.
	 */
	private void WriteToFile() {
		//sw = new StreamWriter(Application.dataPath + "/Saved_High_Score.txt");
		for (int i = 1; i <= currentScores.Count && i <= LeaderboardLength; i++) {
			//sw.WriteLine(currentScores[i - 1]);
			PlayerPrefs.SetString("HighScore" + i + "name", currentScores[i - 1]);
		}
		//sw.Close();
	}
}
