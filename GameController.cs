using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {
	
	public static GameController instance;
	
	private static float Score = 0.0f;
	private static bool hasStarted = false;
	private static bool gameEnded = false;
	
	public static GameObject _ball;
	public static GameObject _player;
	
	public List<string> highScore;
		
	void Start() {
		instance = this;
		
		GameController._ball = GameObject.FindGameObjectWithTag("Ball");
		GameController._player = GameObject.FindGameObjectWithTag("Player");
		
		GameController._ball.SetActive(false);
		GameController._player.SetActive(false);
	}
	
	public static void AddScore(float _value) {
		GameController.Score += _value;
	}
	
	public static float GetScore() {
		return GameController.Score;
	}
	
	void OnGUI() {
		if(!GameController.hasStarted) {
			Rect windowRect0 = new Rect(Screen.width / 2 - 150, Screen.height / 2 - 100, 300, 200);
			windowRect0 = GUI.Window(0, windowRect0, StartWindow, "Start game");
		}
		if(GameController.gameEnded) {
			Rect windowRect1 = new Rect(Screen.width / 2 - 150, Screen.height / 2 - 200, 300, 400);
			windowRect1 = GUI.Window(0, windowRect1, GameOverWindow, "Game ended");
		}
		GUI.Label(new Rect (10, 10, 100, 20), "Score: " + GameController.GetScore().ToString());
	}
	
	private void StartWindow(int windowID) {
		string str1 = "Welcome to 1GAM #May.";
		string str2 = "You have to keep the ball in the air. You can use A and D to move sideways and the arrow keys left and right to rotate the paddle.";
		string str3 = "If you hit the red bricks you gain 5 points instead of only 1 when you hit the paddle.";
		GUILayout.BeginVertical("box");
		GUILayout.TextArea(str1 + "\n\n" + str2 + "\n\n" + str3, 300);
        if (GUI.Button(new Rect(100, 165, 100, 25), "Begin")) {
			StartGame();
		}
        GUILayout.EndVertical();
    }
	
	private void GameOverWindow(int windowID) {
		string str1 = "The game ended.";
		string str2 = "You scored: " + GameController.GetScore();
		
		GUILayout.BeginVertical("box");
		GUILayout.TextArea(str1 + "\n\n" + str2);
		GUILayout.Label("Highscore:");
		foreach(string score in highScore) {
			GUILayout.Box(score);
		}
        if (GUI.Button(new Rect(100, 365, 100, 25), "Restart")) {
			GameController.Reset();
			Application.LoadLevel(Application.loadedLevel);
		}
        GUILayout.EndVertical();
    }
	
	public static void Reset() {
		Score = 0;
		gameEnded = false;
		hasStarted = false;
	}
	
	public static void EndGame() {
		GameController._ball.SetActive(false);
		GameController._player.SetActive(false);
		GameController.gameEnded = true;
		
		SaveScore scoreCont = GameController.instance.gameObject.GetComponent<SaveScore>();
		scoreCont.SaveHighScore(int.Parse(GameController.GetScore().ToString()));
		GameController.instance.highScore = scoreCont.GetHighScoreList();
	}
	
	public static void StartGame() {
		GameController._ball.SetActive(true);
		GameController._player.SetActive(true);
        GameController.hasStarted = !hasStarted;
	}
}
