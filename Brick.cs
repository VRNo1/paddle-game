using UnityEngine;
using System.Collections;

public class Brick : MonoBehaviour {
	
	// Force values
	public float sensitivity = 0.5f;
	public float addedForce = 300.0f;
	
	// Game score
	public float scoreValue = 5.0f;
	
	void OnCollisionEnter( Collision col ) {
		foreach (ContactPoint contact in col.contacts) {
			if( contact.thisCollider == collider ) {
				// This is the paddle's contact point
				// float multiplier = (contact.point.x - transform.position.x) * sensitivity;
				
				// Add force to the ball
				contact.otherCollider.rigidbody.AddForce(addedForce, 0, 0);
				
				// Add game score for the hit.
				GameController.AddScore(scoreValue);
				
				// Play sound
				gameObject.audio.Play();
			}
		}
	}
}
